// BÀI 1: TÍNH LƯƠNG
/*
step 
b1: đặt sự kiện onclick cho nút button để bắt đầu hàm
b2 : Tạo biến cho để người dùng nhập Số Ngày Làm  kiểu dữ liệu Number và lấy giá trị người dùng nhập vào để xử lý bài toán
b3 : Tạo biến chứa tiền lương kiểu dữ liệu number 
b4 : xử lý bài toán ta có tiền lương = số ngày làm * lương 1 ngày;
b5 : In tiền lương ra màn hình thông qua tính chất của inerHTML
*/

var luong1ngay = document.getElementById("luong1ngay").innerHTML;
var btntinhluong = document.getElementById("btntinhluong");
btntinhluong.onclick = function () {
  //input: songaylam:number
  var soNgayLam = document.getElementById("songaylam").value;
  //output: tienluong:number
  var tienLuong = 0;
  //progress
  var luongMotNgay = Number(document.getElementById("luong1ngay").innerHTML);
  tienLuong = soNgayLam * luongMotNgay;
  document.getElementById("tienluong").innerHTML = tienLuong.toLocaleString();
};
/*---------------------------------------------------------------------------------- */
// Bài 2 tính trung bình
/*
step
b1: Đặt nút sự kiện onclick cho nút button
b2: Tạo biến cho người dùng nhập vào năm số kiểu dữ liệu number, và lấy giá trị người dùng nhập vào để xử lý bài toán
b3: Tạo biến chứa giá trị trung bình của 5 số nhập vào kiểu dữ liệu number
b4 : Xử lý bài toán ta có giá trị trung bình của 5 số = (số 1 + số 2 + số 3 + số 4 + số 5) / 5
b5: In giá trị trung bình của 5 số ra màn hình thông qua tính chất của inerHTML
*/
var btntinhtrungbinh = document.getElementById("btntinhtrungbinh");
btntinhtrungbinh.onclick = function () {
  //input somot,sohai,soba,sobon,sonam : number
  var soMot = document.getElementById("somot").value;
  soMot = Number(soMot);
  var soHai = document.getElementById("sohai").value;
  soHai = Number(soHai);
  var soBa = document.getElementById("soba").value;
  soBa = Number(soBa);
  var soBon = document.getElementById("sobon").value;
  soBon = Number(soBon);
  var soNam = document.getElementById("sonam").value;
  soNam = Number(soNam);
  //output giatritrungbinh:number
  var giaTriTrungBinh = 0;
  //progress
  giaTriTrungBinh = (soMot + soHai + soBa + soBon + soNam) / 5;
  document.getElementById("giatritrungbinh").innerHTML = giaTriTrungBinh;
};
/*---------------------------------------------------------------------------------- */
// BÀI 3: TÍNH CHU VI VÀ DIỆN TÍCH
/**
 * Step
 * b1: Đặt nút sự kiện onclick cho nút button
 * b2: tạo biến cho người dùng nhập vào số tiền muốn chuyển đổi(nhapTien) kiểu dữ liệu number và lấy giá trị người dùng nhập vào để xử lý bài toán
 * b3: tạo biến chứa số tiền dã chuyển đổi(raTien) xong kiểu dữ liệu Number
 * b4: Xử lý bài toán số tiền đã chuyển đổi = số tiền muốn chuyển đổi * 23500
 * b5 :In giá trị của số tiền đã chuyển đổi ra màn hình thông qua tính chất của inerHTMLs
 */

var btnquydoi = document.getElementById("btnquydoi");
btnquydoi.onclick = function () {
  // input : nhaptien:number
  var nhapTien = document.getElementById("nhaptien").value;
  nhapTien = Number(nhapTien);
  //output : ratien:number
  var raTien = 0;
  //progress
  raTien = nhapTien * 23500;
  document.getElementById("ratien").innerHTML = raTien.toLocaleString();
  document.getElementById("hienthinhaptien").innerHTML = nhapTien;
};
/*---------------------------------------------------------------------------------- */
// BÀI 4: TÍNH CHU VI VÀ DIỆN TÍCH
/*
step
b1: Đặt nút sự kiện onclick cho nút button
b2: tạo biến cho người dùng nhập vào chiều dài và chiểu rộng kiểu dữ liệu number và lấy giá trị người dùng nhập vào để xử lý bài toán 
b3: tạo biến chứa chu vi và diện tích kiểu dữ liệu Number
b4: xử lý bài toán chu vi = (Chiều dài + Chiều rộng) *2
                Diện tích = chiều dài * chiều rộng
b5: In giá trị của chu vi và diện tích ra màn hình thông qua tính chất của inerHTML
*/
var btntinh = document.getElementById("btntinh");
btntinh.onclick = function () {
  //input:  CHieudai: number , chieu rong:number
  var chieuDai = document.getElementById("chieudai").value;
  chieuDai = Number(chieuDai);
  var chieuRong = document.getElementById("chieurong").value;
  chieuRong = Number(chieuRong);
  // output dientich:number , chuvi:number
  var chuVi = 0;
  var dienTich = 0;
  // progress
  chuVi = (chieuDai + chieuRong) * 2;
  dienTich = chieuDai * chieuRong;
  document.getElementById("chuvi").innerHTML = chuVi;
  document.getElementById("dientich").innerHTML = dienTich;
};
/*---------------------------------------------------------------------------------- */
// Bài 5: TỔNG KÝ SỐ
/*
STEP 
b1: Đặt nút sự kiện onclick cho nút button
b2: tạo biến cho người dùng nhập số có 2 chữ số kiểu dữ liệu number và lấy giá trị người dùng nhập vào để xử lý bài toán
b3: tạo biến chứa tổng ký số kiểu dữ liệu Number
b4: xử lý bài toán tổng ký số = hàng chục + hàng đơn vị
        hàng đơn vị = số người dùng nhập % 10
        hàng chục = (số người dùng nhâp / 10) - (hàng đơn vị / 10)
b5: In giá trị của Tổng ký số ra màn hình thông qua tính chất của inerHTML
*/
var btntinhkyso = document.getElementById("btntinhkyso");
btntinhkyso.onclick = function () {
  // alert(123)
  // input nhapso:number
  var nhapSo = document.getElementById("nhapso").value;
  //output tong:number
  var tong = 0;
  //progress
  var hangChuc = 0;
  var hangDonvi = 0;
  hangDonVi = nhapSo % 10;
  hangDonVi = Number(hangDonVi);
  hangChuc = nhapSo / 10 - hangDonVi / 10;
  hangChuc = Number(hangChuc);
  tong = hangChuc + hangDonVi;
  document.getElementById("tong").innerHTML = tong;
};
